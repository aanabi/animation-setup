version: '2'
services:
    data:
      image: cogniteev/echo
      # This is the path to your project files (..), will map to /var/app in the container
      volumes:
        - ../..:/var/app
        - /data/elasticsearch:/var/elasticsearch

    php-cli:
      image: arkanmgerges/php-cli:1.0

    php-ssh:
      image: arkanmgerges/php-ssh:1.0
      ports:
        - "1122:22"
      volumes_from:
        - data

    php-fpm:
      image: arkanmgerges/php-fpm:1.0
      working_dir: /var/app
      volumes_from:
        - data
      # This is where you can collect xdebug profiling data (set your path), this will map to the /tmp in the container
      volumes:
        - ../.tmp:/tmp
      networks:
        - net01
      environment:
        XDEBUG_CONFIG: "remote_host=b2cv2.lsexprod.int"

    web-nginx:
      image: nginx:latest
      depends_on:
        - php-fpm
      ports:
        - "8082:80"
      volumes_from:
        - data
      volumes:
        - ./config/nginx/site.conf:/etc/nginx/conf.d/site.conf
      networks:
        - net01

    redis:
      container_name: redis01
      image: redis:3.0.7
      ports:
        - "6379:6379"

    # App
    elasticsearch01:
      container_name: es01
      image: elasticsearch:5.0.1
      volumes_from:
        - data
      volumes:
          - ./config/elasticsearch/app/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml
      ports:
        - "9200:9200"
        - "9300:9300"
      networks:
        - net02

    # Log
    elasticsearch02:
      container_name: es02
      image: elasticsearch:5.0.1
      volumes_from:
        - data
      volumes:
          - ./config/elasticsearch/log/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml
      ports:
        - "9400:9200"
      networks:
        - net03

    kibana-log:
      image: kibana:5.0.1
      networks:
        - net03
      ports:
        - "5602:5601"
      environment:
        ELASTICSEARCH_URL: http://192.168.99.100:9400

    kibana:
      image: kibana:5.0.1
      networks:
        - net02
      ports:
        - "5601:5601"
      environment:
        ELASTICSEARCH_URL: http://192.168.99.100:9200

networks:
    net01:
      driver: bridge
    net02:
      driver: bridge
    net03:
      driver: bridge